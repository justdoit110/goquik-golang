package main

import (
  "github.com/joho/godotenv"
  _ "github.com/joho/godotenv"
  "goquik/grpc_client_manager"
  "log"
)

func main()  {
  _ = godotenv.Load("./.env.bot")

  grpcManager := grpc_client_manager.NewGRpcClientManager()

  grpcManager.GRpcConn()

  // поидее это нужно сделать как-то иначе
  defer grpcManager.Conn.Close()

  log.Println("Bot runs...")

  theBot := new(Bot)
  theBot.gRpc = grpcManager
  theBot.Run()
}

