package main

import (
  "encoding/json"
  "fmt"
  "github.com/Knetic/govaluate"
  "github.com/valyala/fasthttp"
  "goquik/grpc_client_manager"
  "io"
  "log"
  "net/url"
  "os"
  "runtime"
  "strconv"
  "strings"
  "time"
)

// "объект" сообщения от пользователя
type Message struct {
  Id    int `json:"message_id"`
  Text  string `json:"text"`
  From  MessageFrom `json:"from"`
}

// (под)структура с инфой "от кого"
type MessageFrom struct {
  Id  int `json:"id"`
  LastName  string  `json:"last_name"`
}

// объект/элемент в массиве Updates объекта ApiBotResponse
// https://core.telegram.org/bots/api#update
type Update struct {
  UpdateId  int `json:"update_id"`
  MessageObj  Message `json:"message"`

}

// базовая структура возвращаемая apiBot методом getUpdates
type ApiBotResponse struct {
  Ok  bool  `json:"ok"`
  Updates []Update  `json:"result"`
}

// структура записи оповещения по цене
type Alert struct {
  Tool  string
  Condition string
  Value float64
  Recipient int
}

// основная структура(класс) бота
type Bot struct {
  ApiBotResp ApiBotResponse // ответ от api.telegram
  ApiBaseUrl string
  ScheduledAlerts []Alert   // массив запланированных оповещений
  QuotesServStatus bool     // онлайн статус сервера/сервиса котировок
  gRpc *grpc_client_manager.GRpcClientManager         // менеджер grpc методов
}

// aka main()
func (bot *Bot ) Run()  {

  // берем token из .env файла
  botToken, ok := os.LookupEnv("BOT_TOKEN")
  if !ok {
    log.Println("Undefined bot api_token\nBot exit...")
    return
  }
  bot.ApiBaseUrl = "https://api.telegram.org/bot"+botToken+"/"

  // проверяет приходят ли котировки реалтайм
  // или quik выключен и новых данных нет
  go bot.MonQuotesServStatus()

  // запускаем обработку заявок на оповещение
  go bot.processScheduledAlerts()

  // нон-стоп запрос на наличие и обработку новых сообщений
  // из клиента Телеграм
  bot.processMessages()
}


// запрашиывает в api.telegram наличие новых сообщений
// отправляет на обработку новые сообщения
func (bot *Bot ) processMessages()  {

  // lastUpdateId (int)
  lu := 0

  // нон-стоп запрос на наличие новых сообщений и их обработка
  // интервал 1 сек.
  for {

    // id предыдущего/последнего запрашиваемого сообщения
    lastUpdate := bot.getLastUpdateId()

    _, respBody, err := fasthttp.Get(nil, bot.ApiBaseUrl+"getUpdates?offset="+lastUpdate)

    if err != nil {
      log.Println("botApi request err: ", err)
      continue
    }

    err = json.Unmarshal(respBody, &bot.ApiBotResp)
    if err != nil {
      fmt.Println("Err Unmarshal ApiResponse: ", err)
    }

    if len(bot.ApiBotResp.Updates)>0 {
      for _,update := range bot.ApiBotResp.Updates {

        go bot.processMsg(&update.MessageObj)

        lu = update.UpdateId
      }
      bot.setLastUpdateId(lu+1)
    } else {
      //log.Println("no updates")
    }

    runtime.Gosched()
    time.Sleep(time.Duration(1*time.Second) )
  }
}


// обработка объекта сообщения пользователя
func (bot *Bot ) processMsg(msg *Message ) {

  if !bot.QuotesServStatus {
    bot.sendMessage("Now offline. Try later.", msg.From.Id)
    return
  }

  if msg.Text[0] == '/' {
    bot.processCmd(msg)
    return
  }

  bot.sendMessage("Available commands:\n /show\n/if", msg.From.Id)
}

// обработка командных сообщений пользователя
func (bot *Bot ) processCmd(msg *Message ) {

  cmd := strings.Fields(msg.Text)

  switch cmd[0] {
  case "/show":
    bot.respAvailableTools(msg.From.Id)
  case "/if":
    bot.setAlert(cmd, msg.From.Id)
  default:
    bot.sendMessage("Available commands:\n /show\n/if", msg.From.Id)
  }
}

// отправляет пользователю список доступных инструментов (торговли)
func (bot *Bot ) respAvailableTools(chat int) interface{} {

  var tools []string
  quotesList,_ := bot.gRpc.GetQuotes()
  tools = append(tools, "```\ntool  =>  current price\n")
  for key, value := range quotesList {
    tools = append(tools, fmt.Sprintf("%-6s=>%8s", strings.ToLower(key), value))
  }

  tools = append(tools,"```")
  text := strings.Join(tools, "\n")

  bot.sendMessage(text, chat)
  return nil
}

// создает,пишет в "хранилище" (ScheduledAlerts)
// заявки на оповещение (при достижении условия цены по инструменту)
func (bot *Bot ) setAlert(cmd []string, chatId int)  {
  if len(cmd)<4 {
    bot.sendMessage("Синтаксис команды: \n/if *код_инструмента* _условие(>,>=,<,<=)_ *цена*\nНапр: /if brn9 > 65.12", chatId)
    return
  }

  conditions := []string{"<", ">", "<=", ">="}
  if !includes(conditions, cmd[2]) {
    bot.sendMessage("wrong condition (use '<', '>', '<=', '>=')", chatId)
    return
  }

  quotesList,_ := bot.gRpc.GetQuotes()
  if _, ok := quotesList[strings.ToLower(cmd[1])]; !ok {
    bot.sendMessage("Unknown trade tool: "+cmd[1]+". (Use cmd '/show' to see all tools)", chatId)
    return
  }

  val, err := strconv.ParseFloat(cmd[3], 32)
  if err!=nil {
    bot.sendMessage("It looks like the value - '"+cmd[3]+"' is wrong. Must be number e.g. 23, 34.03", chatId)
    return
  }

  bot.ScheduledAlerts = append(bot.ScheduledAlerts, Alert{cmd[1], cmd[2], val, chatId})
  bot.sendMessage("Alert added. You will notice when "+cmd[1]+cmd[2]+cmd[3], chatId)

}

// отправка сообщение клиенту
func (bot *Bot ) sendMessage(msgText string, chatId int)  {
  params := url.Values{}
  params.Add("text", msgText)
  params.Add("parse_mode", "Markdown")
  params.Add("chat_id", strconv.Itoa(chatId))

  uri := bot.ApiBaseUrl+"sendMessage?"+params.Encode()
  _, _, err := fasthttp.Get(nil, uri)
  if err != nil {
    fmt.Printf("Request error(uri=%s): %s", uri, err)
  }
}

// цикл обработки "хранилища" заявок на оповещение
// интервал 0.5сек
func (bot *Bot ) processScheduledAlerts() {
  param := make(map[string]interface{}, 2)
  var (
    expr *govaluate.EvaluableExpression
    err error
    cmpRes interface{}
    toolVal float64
  )

  for {

    if len(bot.ScheduledAlerts)==0 {
      runtime.Gosched()
      time.Sleep(500*time.Millisecond)
      continue
    }

    quotesList,_ := bot.gRpc.GetQuotes()

    for i,alert := range bot.ScheduledAlerts {

      toolVal,_ = strconv.ParseFloat(quotesList[strings.ToLower(alert.Tool)], 32)

      expr, err = govaluate.NewEvaluableExpression("a "+alert.Condition+" b")
      if err != nil {
        log.Println("Error govaluate expression: ", err)
        continue
      }

      param["a"] = toolVal
      param["b"] = alert.Value

      cmpRes, err = expr.Evaluate(param)
      if err != nil {
        log.Println("Error govaluate Evaluate: ", err)
        continue
      }

      if cmpRes.(bool) {

        msg := fmt.Sprintf("Current price of *%s* = _%.2f_.\nYou asked: *%s %s %.3f*", alert.Tool, toolVal, alert.Tool, alert.Condition, alert.Value)
        bot.sendMessage( msg, alert.Recipient)
        // удаляет обработанную заявку из хранилища
        bot.ScheduledAlerts = append(bot.ScheduledAlerts[:i], bot.ScheduledAlerts[i+1:]...)
      }

    }
    runtime.Gosched()
    time.Sleep(500*time.Millisecond)
  }

}

// проверяет приходят ли котировки реалтайм
// или quik выключен и новых данных нет
func (bot *Bot ) MonQuotesServStatus() {

  for {

    bot.QuotesServStatus = bot.gRpc.GetServStatus()

    runtime.Gosched()
    time.Sleep(time.Duration(3*time.Second))
  }

}

// получаем последний обработанный updateId
// хранится в файле @todo взмжн для хранения стоило бы заюзать что-то другое
func (bot *Bot ) getLastUpdateId() string {

  fp, err := os.Open("./last_update.txt")
  if err != nil {
    os.Create("./last_update.txt")
    return "0"
  }
  defer fp.Close()

  var lastUpdate string
  _, err = fmt.Fscanf(fp, "%s", &lastUpdate)
  if err != nil {
    if err!=io.EOF {
      log.Println("Error read from last_update.txt", err)
    }
    return "0"
  }

  return lastUpdate
}

// пишем последний обработанный updateId
// хранится в файле @todo взмжн для хранения стоило бы заюзать что-то другое
func (bot *Bot ) setLastUpdateId(val int) {
  fp, err := os.OpenFile("./last_update.txt", os.O_WRONLY, 0)
  if err != nil {
    log.Println("Error open last_update.txt (O_WRONLY): ", err)
  }
  defer fp.Close()

  a := strconv.Itoa(val)

  _, err = fp.Write([]byte(a))
  if err!=nil {
    log.Println("Error write to last_update.txt (O_WRONLY): ", err)
  }
}

// helper, типо аналог in_array (PHP), Array.includes (JS)
func includes(a []string, b string) bool {
  for _, v := range a {
    if v == b {
      return true
    }
  }
  return false
}

