package grpc_client_manager

import (
  "context"
  "errors"
  "google.golang.org/grpc"
  "goquik/grpc_quotes"
  "log"
  "os"
)

type GRpcClientManager struct {
  grpcClientConn quotes.QuotesManagerClient //QuotesManager gRPC Client
  Conn *grpc.ClientConn // просто gRPC connection
}

func NewGRpcClientManager() *GRpcClientManager  {
  return &GRpcClientManager{}
}

// подключаемся к QuotesManagerServer'у
func (m *GRpcClientManager) GRpcConn() {

  //
  grpcHost, hostOk := os.LookupEnv("GRPC_CONN_HOST")
  grpcPort, portOk := os.LookupEnv("GRPC_CONN_PORT")
  if !hostOk || !portOk {
    log.Fatalln("undefined GRPC_CONN_PORT or GRPC_CONN_HOST in .env.bot, exit")
    return
  }

  conn, err := grpc.Dial(grpcHost+":"+grpcPort, grpc.WithInsecure())
  if err != nil {
    log.Fatalf("did not connect: %v", err)
  }

  m.Conn = conn

  m.grpcClientConn = quotes.NewQuotesManagerClient(m.Conn)
}


// запрашивает текущие котировки
func (m *GRpcClientManager) GetQuotes() (map[string]string, error) {

  ctx := context.Background()

  res, err := m.grpcClientConn.Get(ctx, &quotes.Empty{})
  if err != nil {
    //log.Println()
    return nil, errors.New("failed getting quotes")
  }

  return res.QuotesList, nil
}

// запрашивает статус обновления котировок (true/false)
func (m *GRpcClientManager) GetServStatus() bool {

  ctx := context.Background()

  res, err := m.grpcClientConn.GetStatus(ctx, &quotes.Empty{})
  if err != nil {
    log.Println("failed getting quotes_server status...")
  }

  return res.Online
}