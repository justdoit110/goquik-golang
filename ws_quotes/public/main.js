let codes = [];

window.addEventListener("load", function(evt) {

    var print = function(message) {
        console.log("n: ", message)
    };
    var ws;

    if (ws) {
        return false;
    }

    var loc = window.location, new_uri;
    if (loc.protocol === "https:") {
        new_uri = "wss:";
    } else {
        new_uri = "ws:";
    }

    new_uri += "//" + loc.host;
    new_uri += loc.pathname + "ws";
    ws = new WebSocket(new_uri);
    ws.onopen = function(evt) {
        print("OPEN");
    };
    ws.onclose = function(evt) {
        print("CLOSE");
        ws = null;
    };

    ws.onmessage = function(evt) {
        buildDOM(evt.data)
    };

    ws.onerror = function(evt) {
        print("ERROR: " + evt.data);
    };

});

renderCodes = (codes) => {
    let root = document.getElementById("root");
    codes.forEach(i=>{
      let el = document.createElement("div");
      el.innerHTML = `<p>${i}</p>`;
      root.appendChild(el)
    })
};

buildDOM = (data) => {
    let obj = data.split("#");

    obj.forEach(item=>{
        let q = item.split(":");

        if(q.length < 2) return;
        if(q[0]==="offline") {
            offline();
            return;
        }

        if(!codes.includes(q[0])) {
            codes.push(q[0]);
            buildQuoteDom(q[0]);
        }
        updateQuoteValue({sec_code: q[0], value: q[1]});
        online();
    })
};

buildQuoteDom = sec_code => {
    let root = document.getElementById("root-table");
    let el = document.createElement("tr");
    el.setAttribute("id", sec_code.toLowerCase());
    el.innerHTML = `<td class="code-name">${sec_code.toUpperCase()}</td><td>:</td><td class="value">-</td>`;
    root.appendChild(el)
};

updateQuoteValue = quote => {
    document.querySelector(`#${quote.sec_code.toLowerCase()}>.value`).innerText = quote.value;
};

offline = () =>{
    setStatus("<b>OFFLine</b>");
};

online = () =>{
    setStatus("<i>OnLine</i>");
};

setStatus = (status = "") => {
    document.querySelector("#root>.status").innerHTML = status;
};