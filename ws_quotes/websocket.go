package main

import (
  "github.com/joho/godotenv"
  "goquik/grpc_client_manager"
)


func main()  {

  _ = godotenv.Load(".env.ws")


  gRpcClientManager := grpc_client_manager.NewGRpcClientManager()

  gRpcClientManager.GRpcConn()

  // поидее это нужно сделать как-то иначе
  defer gRpcClientManager.Conn.Close()

  ws := new(wsQuotes)

  ws.gRpc = gRpcClientManager

  ws.Run()
}
