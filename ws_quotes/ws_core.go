package main

import (
  "github.com/gorilla/websocket"
  "goquik/grpc_client_manager"
  "log"
  "net/http"
  "os"
  "runtime"
  "strings"
  "time"
)

type wsQuotes struct {
  clients   map[*websocket.Conn]bool  // подключенные клиенты
  broadcast chan string               // канал передачи котировок
  upgrader  websocket.Upgrader
  gRpc *grpc_client_manager.GRpcClientManager         // менеджер вызова grpc методов

}

func (wsq *wsQuotes ) Run()  {


  httpHost, hostOk := os.LookupEnv("HTTP_SERV_HOST")
  httpPort, portOk := os.LookupEnv("HTTP_SERV_PORT")
  if !hostOk || !portOk {
    log.Fatalln("undefined HTTP_SERV_HOST or HTTP_SERV_PORT in .env.bot, exit")
    return
  }

  wsq.upgrader = websocket.Upgrader{}
  wsq.clients = make(map[*websocket.Conn]bool)
  wsq.broadcast = make(chan string)


  // файловый сервер который вернет html&js
  fs := http.FileServer(http.Dir("./public"))
  http.Handle("/", fs)

  // websocket роут
  http.HandleFunc("/ws", wsq.handleConnections)

  // читаем из канала и отпр. всем клиентам
  go wsq.sendMessages()

  // формирует сообщение с котировками и пишет в канал
  go wsq.bcMessage()

  // Start server
  log.Println("http server started on "+httpHost+":"+httpPort)
  err := http.ListenAndServe(httpHost+":"+httpPort, nil)
  if err != nil {
    log.Fatal("ListenAndServe: ", err)
  }
}


// обработка ws connections
func  (wsq *wsQuotes ) handleConnections(w http.ResponseWriter, r *http.Request) {
  // Upgrade http соединение на websocket
  ws, err := wsq.upgrader.Upgrade(w, r, nil)
  if err != nil {
    log.Fatal(err)
  }

  defer ws.Close()

  log.Println("WS: someone connected...")

  // запишем клиента
  wsq.clients[ws] = true

  for {

    err := ws.ReadJSON(nil)
    // если не удалось ничего прочитать - закрываем соединение
    if err != nil {
      log.Printf("error: %v", err)
      delete(wsq.clients, ws)
      break
    }
    runtime.Gosched()
    time.Sleep(700 * time.Millisecond)
  }
}

// рассылка сообщений клиентам
func  (wsq *wsQuotes ) sendMessages() {
  for {
    // Grab the next message from the broadcast channel
    msg := <-wsq.broadcast
    // Send it out to every client that is currently connected
    for client := range wsq.clients {
      err := client.WriteMessage(websocket.TextMessage, []byte(msg))
      if err != nil {
        log.Printf("error: %v", err)
        client.Close()
        delete(wsq.clients, client)
      }
    }
  }
}

// через gRPC берет котировки и пришет в канал
// (где они читаются и отпр. sendMessages() )
func  (wsq *wsQuotes ) bcMessage()  {

  for {
    if wsq.gRpc.GetServStatus() == false {
      wsq.broadcast <- "offline:1"
    } else {
      var res []string

      quotesList,_ := wsq.gRpc.GetQuotes()

      for k,v := range quotesList {
        res = append(res, k+":"+v)
      }
      wsq.broadcast <- strings.Join(res, "#")
    }

    runtime.Gosched()
    time.Sleep(500*time.Millisecond)
  }
}