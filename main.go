package main

import (
  "bufio"
  "context"
  "fmt"
  _ "github.com/joho/godotenv/autoload"
  "google.golang.org/grpc"
  "goquik/grpc_quotes"
  "log"
  "net"
  "os"
  "strings"
  "sync"
  "time"
)

// conf TCP
const (
  //CONN_HOST = "85.143.174.255"
  TCP_CONN_HOST = "0.0.0.0"
  TCP_CONN_PORT = "3333"
  TCP_CONN_TYPE = "tcp"
)


// статус реалТайм обновления котировок
// (т.е. когда из Quik'a приходят данные)
var TcpUpdateTime int64

var mtx = &sync.Mutex{}

type QuotesManager struct {
  mu sync.RWMutex
  QuotesList map[string]string
}

func (q *QuotesManager ) Get(ctx context.Context, empty *quotes.Empty) (*quotes.QuotesMsg, error)  {
  return &quotes.QuotesMsg{QuotesList: q.QuotesList}, nil
}

func (q *QuotesManager ) GetStatus(ctx context.Context, empty *quotes.Empty) (*quotes.Status, error) {
  status := true
  if time.Now().Unix() - TcpUpdateTime > 5 {
    status = false
  }
  return &quotes.Status{Online: status}, nil
}

var qMan *QuotesManager

func main() {

  TcpUpdateTime = time.Now().Unix()

  qMan = &QuotesManager{
    mu: sync.RWMutex{},
    QuotesList: map[string]string{},
  }

  go RunGRpcServ()
  RunTCPServe()
}

// запускает gRpc сервер
func RunGRpcServ()  {

  grpcHost, hostOk := os.LookupEnv("GRPC_SERV_HOST")
  grpcPort, portOk := os.LookupEnv("GRPC_SERV_PORT")
  if !hostOk || !portOk {
    log.Println("Undefined GRPC_SERV_PORT or GRPC_SERV_HOST in .env, exit...")
    return
  }

  lis, err := net.Listen("tcp", grpcHost+":"+grpcPort)
  if err != nil {
    log.Fatalf("failed to listen: %v", err)
  }
  s := grpc.NewServer()
  quotes.RegisterQuotesManagerServer(s, qMan)
  log.Printf("GrpcServer starting on %s ...", grpcHost+":"+grpcPort)
  if err := s.Serve(lis); err != nil {
    log.Fatalf("failed to serve: %v", err)
  }
}

// на TCP сервер QUIK будет слать данные
func RunTCPServe()  {
  // Listen for incoming connections.
  l, err := net.Listen(TCP_CONN_TYPE, TCP_CONN_HOST+":"+TCP_CONN_PORT)
  if err != nil {
    fmt.Println("Error listening:", err.Error())
    os.Exit(1)
  }
  // Close the listener when the application closes.
  defer l.Close()
  log.Println("TCP listening on " + TCP_CONN_HOST + ":" + TCP_CONN_PORT)
  for {
    // Listen for an incoming connection.
    conn, err := l.Accept()
    if err != nil {
      fmt.Println("Error accepting: ", err.Error())
      os.Exit(1)
    }
    // Handle connections in a new goroutine.
    go handleRequest(conn)
  }
}

// Handles TCP incoming requests.
func handleRequest(conn net.Conn) {

  defer conn.Close()
  var (
    text string
    quotesArr []string
  )

  scanner := bufio.NewScanner(conn)
  for scanner.Scan() {
    text = scanner.Text()
    if text == "exit" {
      break
    } else if text != "" {

      // text = key:value
      quotesArr = strings.Split(text, ":")
      // лайтовая валидация
      if len(quotesArr)>1 {

        key := strings.ToLower(quotesArr[0])
        mtx.Lock()
        qMan.QuotesList[key] = quotesArr[1]
        mtx.Unlock()
        TcpUpdateTime = time.Now().Unix()
      }

    }
  }

}
